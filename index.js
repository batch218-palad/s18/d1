// console.log("Hello world!");

// function printInfo(){
// 	let nickname = prompt("Enter your nickname: ");
// 	console.log("Hi, " + nickname);
// }

// printInfo();

				// parameter
function printName(firstName){
	console.log("My name is " + firstName);
}

printName("Juana"); // argument
printName("John");
printName("Cena");

// Now we have a reusable function / task but could have different output base on what value to process w/ the help of 
//  [SECTION] Parameters and Arguments

//  Parameter
	// "firstName" is called a parameter
		// "parameter" acts as a named variable/container that exists only inside a function
		// It is used to store information that is provided to a function when it is called/invoked

// Argument
	// "Juana", "John", and "Cena" the information/data provided directly into the function is called "argument"
	// Values passed when invoking a function are called arguments
	// These arguments are then stored as the parameter w/in the function


let sampleVariable = "Inday";

printName(sampleVariable);
// variables can also be passed as an argument

// -----------------

function checkDivisibilityBy8(num){
	let remainder = num % 8;
	console.log("The remainder of "+num+" divided by 8 is: "+remainder)
	let isDivisibilityBy8 = remainder === 0;
	console.log("Is "+num+" divisible by 8?")
	console.log(isDivisibilityBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// [SECTION] Function as argument
	// Function parameters can also accept functions as arguments
	// some complex functions uses other functions to perform more complicated result.

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");
	}

	function invokeFunction(argumentFunction){
		argumentFunction();
	}

	invokeFunction(argumentFunction);
	console.log(argumentFunction);

// -------------------------

// [SECTION] Using Multiple Parameters

	function createFullName(firstName, middleName, lastName){
		console.log("My full name is " + firstName + " " + middleName + " " + lastName);
	}

	createFullName("Rafael", "Acesor", "Palad");

	// Usung variables as an argument
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

	function getDifferenceOf8Minus4(numA, numB){
		console.log("Difference: "+ (numA - numB));
	}

	getDifferenceOf8Minus4(8,4);
	// getDifferenceOf8Minus4(4,8); //THis will result to logical error

// [SECTION] Return Statement

	// The "return" statement allows us to output a value from a function to be passed to the line/blcok of code that is invoked/called
	function returnFullName(firstName, middleName, lastName){
		//return firstName + " " + middleName + " " + lastName;

		
		// We could also create a variable inside the function to contain the result and return the varianle instead
		let fullName = firstName + " " + middleName + " " + lastName;
		return fullName;


		// This line of code will not be printed
		console.log("This is printed inside a function");

	}
	let completeName = returnFullName("Paul", "Smith", "Jordan");
	console.log(completeName);

	console.log("I am "+completeName)



	function printPlayerInfo(userName, level, job){
		console.log("Username: "+ userName);
		console.log("Level: "+ level);
		console.log("Job: " + job);
	}

	let user1 = printPlayerInfo("boxzMapagmahal", "Senior", "Programmer");
	console.log(user1); //returns undefined since no return keyword
	























